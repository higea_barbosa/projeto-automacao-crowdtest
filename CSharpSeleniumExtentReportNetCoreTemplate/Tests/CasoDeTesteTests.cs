using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class CasosDeTesteTests : TestBase
    {

        #region Pages and Flows Objects
        LoginPage loginPage;
        MainPage mainPage;
        CasoDeTestePage casoDeTestePage;
        GerenciarPage gerenciarPage;
        DashboardPage dashboardPage;
        ProjetosPage projetosPage;

        #endregion

        [Test]
        public void ValidarCasoDeTeste()
        {
            loginPage = new LoginPage();
            mainPage = new MainPage();
            casoDeTestePage = new CasoDeTestePage();
            gerenciarPage = new GerenciarPage();
            dashboardPage = new DashboardPage();
            projetosPage = new ProjetosPage();


            #region Parameters
            string usuario = "higea.barbosa@base2.com.br";
            string senha = "KWFkwf469";
            #endregion

            loginPage.PreencherUsuario(usuario);
            loginPage.PreencherSenha(senha);
            loginPage.ClicarEmProsseguir();
            loginPage.ClicarEmEntrar();
            
            gerenciarPage.ClicarEmGerenciar();
            dashboardPage.ClicarEmProjetos();
            projetosPage.SelecionarProjetoDesejado();
            projetosPage.SelecionarCasoDeTeste();
            casoDeTestePage.RetornaIdDoPrimeiroCasoDeTeste();


            Assert.AreEqual("1", casoDeTestePage.RetornaIdDoPrimeiroCasoDeTeste());
        }

    }
}
