using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class DashboardPage : PageBase 
    {
        #region Mapping 
        By projetosButton = By.CssSelector("a[href='/projects']"); 
        #endregion

        #region Actions
        public void ClicarEmProjetos()
        {
            Click(projetosButton);
        }
        #endregion
    }


}