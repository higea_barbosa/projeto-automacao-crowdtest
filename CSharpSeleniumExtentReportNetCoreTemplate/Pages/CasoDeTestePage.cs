using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class CasoDeTestePage : PageBase
    {
                
        #region Mapping
        By idCasoDeTeste = By.XPath("//mat-cell[@role='gridcell'][text()='1']");
        #endregion

        #region Actions 
        public string RetornaIdDoPrimeiroCasoDeTeste()
        {
            WaitForElement(idCasoDeTeste);
            return GetText(idCasoDeTeste);
        }

        #endregion
    }
}
