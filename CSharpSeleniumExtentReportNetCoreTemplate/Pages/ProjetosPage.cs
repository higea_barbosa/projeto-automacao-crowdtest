using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class ProjetosPage : PageBase 
    {
        #region Mapping 
        By selecionaProjeto = By.XPath("//mat-cell[@role='gridcell'][text()='Treinamento - Higea Barbosa']");
        By casoDeTesteButton = By.XPath("//*[@id='mat-tab-label-0-2']/div");
        #endregion

        #region Actions
        public void SelecionarProjetoDesejado()
        {
            WaitForElement(selecionaProjeto);
            Click(selecionaProjeto);
        }
        public void SelecionarCasoDeTeste()
        {
            Click(casoDeTesteButton);
        }
        #endregion
    }


}