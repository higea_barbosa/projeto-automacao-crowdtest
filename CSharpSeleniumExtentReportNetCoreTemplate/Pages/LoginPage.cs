using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class LoginPage : PageBase
    {
        #region Mapping
        By usernameField = By.Id("login");
        By passwordField = By.Id("password");
        By loginButton = By.XPath("//button[text()='ENTRAR']");
        By prosseguirButton = By.LinkText("Prosseguir");
        #endregion

        #region Actions
        public void ClicarEmProsseguir()
        {
            WaitForElement(prosseguirButton);
            Click(prosseguirButton);
        }    
        public void PreencherUsuario(string usuario)
        {
            SendKeys(usernameField, usuario);
        }

        public void PreencherSenha(string senha)
        {
            SendKeys(passwordField, senha);
        }

        public void ClicarEmEntrar()
        {
            Click(loginButton);
        }
        #endregion
    }
}
