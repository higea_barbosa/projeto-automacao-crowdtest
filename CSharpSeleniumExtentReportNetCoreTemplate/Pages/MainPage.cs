﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class MainPage : PageBase
    {
        #region Mapping
        By mensagemBemVindo = By.XPath("//div[@class='row title']//span[text()='Bem-vindo ao Crowdtest! O que deseja fazer hoje?']"); 
        #endregion

        #region Actions
        public string RetornaMensagemBemVindo()
        {
            return GetText(mensagemBemVindo);
        }
        
        #endregion
    }
}
