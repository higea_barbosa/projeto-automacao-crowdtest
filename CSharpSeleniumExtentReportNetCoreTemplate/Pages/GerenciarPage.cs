using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class GerenciarPage : PageBase 
    {
        #region Mapping 
        By gerenciarButton = By.CssSelector(".btn.btn-crowdtest.mr-1:first-child");
        By mensagemBemVindo = By.XPath("//div[@class='row title']//span[text()='Bem-vindo ao Crowdtest! O que deseja fazer hoje?']");
        #endregion

        #region Actions
        public void ClicarEmGerenciar()
        {
            WaitForElement(gerenciarButton);
            Click(gerenciarButton);
        }

         public string RetornaMensagemBemVindo()
        {
            return GetText(mensagemBemVindo);
        }
        #endregion
    }


}